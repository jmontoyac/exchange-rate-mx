FROM python:3.7

# Set container Time Zone
ENV TZ=America/Monterrey
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

COPY *.py /scripts/
COPY *.json /config/
COPY crontab /config/

# Python related stuff
RUN pip install --upgrade pip
RUN pip install twython
RUN pip install urllib3
RUN pip install bs4
RUN pip install datetime
RUN apt update && apt install -y cron

RUN crontab /config/crontab

#CMD ["crond", "-f"]