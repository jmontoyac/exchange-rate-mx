## Utils 

# Gets the distance between two points of coordinates
import firebase_initialize as fb
import math
import json
import datetime
from datetime import date, timedelta
from json import dumps

ROUTE_REFERENCE = 'Rutas/'
ROUTE_PREFIX = 'route_'
AR_PREFIX    = 'ar_'
STOPPED_DISTANCE = 20   # METERS TO BE CONSIDERED STOPPED
STRING_TIME_MESSAGE = '# getdecimal_time astringtime: '

# returns distance between two points in kilometers
def get_distance(lat1, lon1, lat2, lon2):
    radius = 6371 # km

    # Cast coordinates to float
    f_lat1 = float(lat1)
    f_lon1 = float(lon1)
    f_lat2 = float(lat2)
    f_lon2 = float(lon2)

    dlat = math.radians(f_lat2-f_lat1)
    dlon = math.radians(f_lon2-f_lon1)
    a = math.sin(dlat/2) * math.sin(dlat/2) + math.cos(math.radians(f_lat1)) \
        * math.cos(math.radians(f_lat2)) * math.sin(dlon/2) * math.sin(dlon/2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    d = radius * c

    return d

# Gets the Estimated Time of Arrival (ETA)
# dtd = distance to destination
def get_eta(dtd, speed):
    current_time = datetime.datetime.now()
    if speed == 0:
        speed = 30
    time_to_destination = dtd / speed
    eta = current_time + timedelta(hours=time_to_destination)
    return int(eta.timestamp())

def get_eta_n(a_etd, a_start_time):
    etd_seconds = time_string_to_seconds(a_etd)
    start_time_object = datetime.datetime.fromtimestamp(a_start_time)
    
    eta = start_time_object + datetime.timedelta(seconds=etd_seconds)
    return int(eta.timestamp())


# Gets the Estimated Trip Duration
def get_etd(dtd, speed):
    if speed == 0:
        speed = 30
    print('Calculating ETD')
    print('Distance To Destination:', round(dtd, 4))
    print('Speed: ', speed)
    etd = dtd / speed

    hours = int(etd)
    minutes = (etd * 60) % 60
    seconds = (etd * 3600) % 60
    print('Hora: ', hours)
    print('Minuto: ', minutes)
    print('Segundos: ', seconds)

    time_string = "%02d:%02d:%02d" % (hours, minutes, seconds)
    print('Returning ETD time string: ', time_string)
 
    return time_string


# Gets the Estimated Trip Duration
def get_etd_decimal(dtd, speed):
    if speed == 0:
        return 0
    print('Calculating ETD')
    print('Distance To Destination:', round(dtd, 4))
    print('Speed: ', speed)
    etd = dtd / speed

    print(f'Returning ETD: {etd}')
 
    return etd

# Gets the number of rest times for route
# Not used anymore, resti times got form geofences
def get_rest_times(dtd, speed):
    rest_times = 0
    etd = dtd / speed
    if etd > 5 and etd < 10:
        rest_times = rest_times + 1
    elif etd > 10 and etd < 14:
        rest_times = rest_times + 2
    elif etd >= 14:
        rest_times = rest_times + 3
    return rest_times


# Calculate time of route
def calculate_route_time(a_distance, a_speed):
    return a_distance / a_speed 


# Format decimal in hours time as hh:mm:ss
def format_hour(a_decimal_time):
    seconds = a_decimal_time * 3600
    m, s = divmod(seconds, 60)
    h, m = divmod(m, 60)
    return "%02d:%02d:%02d" % (h, m, s)


# Gets the advance in route percentage
def get_advance_percentage(dtd, a_route_length):
    print('Entering get_advance_percentage')
    print('# dtd:', dtd)
    print('# a_route_length:', a_route_length)
    return round((100 - (dtd * 100 / a_route_length)), 2)


# Get the elapsed time (in hours) in route between two epoch times
def get_elapsed_time_hours(a_initial_time, a_current_time):
    a_initial_time = int(a_initial_time)
    a_current_time = int(a_current_time)
    print('Get elapsed time initial: ', a_initial_time)
    print('Get elapsed time current time: ', a_current_time)
    # Convert epoch times to datetime objects
    my_initial_time = datetime.datetime.fromtimestamp(a_initial_time)
    my_current_time = datetime.datetime.fromtimestamp(a_current_time)

    # Calculate difference, convert to seconds and return in hours format
    my_dif = my_current_time - my_initial_time
    my_dif_hours = round((my_dif.total_seconds() / 3600), 2)
    return format_hour(my_dif_hours)


# Get the elapsed time (in decimal) in route between two epoch times
def get_elapsed_time_decimal(a_initial_time, a_current_time):
    a_initial_time = int(a_initial_time)
    a_current_time = int(a_current_time)
    print('Get elapsed time initial: ', a_initial_time)
    print('Get elapsed time current time: ', a_current_time)
    # Convert epoch times to datetime objects
    my_initial_time = datetime.datetime.fromtimestamp(a_initial_time)
    my_current_time = datetime.datetime.fromtimestamp(a_current_time)

    # Calculate difference, convert to seconds and return in hours format
    my_dif = my_current_time - my_initial_time
    my_dif_hours = round((my_dif.total_seconds() / 3600), 2)
    return my_dif_hours

# Get time (in decimal hours) advance percentage in route
def get_time_percentage(a_elapsed_time, a_route_time):
    print('Entering get_time_percentage')
    if a_route_time == 0:
        a_route_time = 1
    print('# a_elapsed_time: ', a_elapsed_time)
    print('# a_route_time: ', a_route_time)
    return round((a_elapsed_time * 100 / a_route_time), 2)


# not used anymore, does not support times greter tha 24 hours
def get_dec_time(a_string_time):
    print(STRING_TIME_MESSAGE, a_string_time)
    date_time_obj = datetime.datetime.strptime(a_string_time, '%H:%M:%S')
    total_seconds = date_time_obj.hour * 3600 + date_time_obj.minute *60 + date_time_obj.second
    return round(total_seconds / 3600, 4)

# Get hour in decimal number, ie. 2:45 is 2.75
# Receives time in HH:MM:SS format
def get_decimal_time(a_string_time):
    print(STRING_TIME_MESSAGE, a_string_time)
    ftr = [3600, 60, 1]
    return round(sum([a*b for a,b in zip(ftr, map(int,a_string_time.split(':')))]) / 3600, 4)

# Get number of seconds from time string, ie. 2:45 is 2.75
# Receives time in HH:MM:SS format
def time_string_to_seconds(a_string_time):
    print(STRING_TIME_MESSAGE, a_string_time)
    ftr = [3600, 60, 1]
    return round(sum([a*b for a,b in zip(ftr, map(int,a_string_time.split(':')))]), 4)

def epoch_to_date(a_epoch_date):
    return datetime.datetime.fromtimestamp(a_epoch_date)

def decimal_to_time_string(a_decimal_time):
    hours = int(a_decimal_time)
    minutes = (a_decimal_time*60) % 60
    seconds = (a_decimal_time*3600) % 60

    return ("%02d:%02d:%02d" % (hours, minutes, seconds))

# Get the current tracking positions
def get_last_position(an_imei):
    json_response = ''
    an_imei = an_imei = an_imei.replace('"', '')
    my_ref = 'Tracking/' + an_imei
    ref = fb.db.reference(my_ref)
    print('Getting position for IMEI: ', an_imei)

    if an_imei == '778499170000001':
        print('Test IMEI is invalid: ', an_imei, ' returning.')
        return json_response

    snapshot = ref.order_by_child('date').limit_to_last(1).get()
    if snapshot != None:
        for key, val in snapshot.items():
            json_string = {'latitud':str(val['Lat']),'longitud':str(val['Lon']),'fecha':str(val['date'])}
            json_response = json.dumps(json_string)
        return json_response
    else:
        return json.dumps('Err')

# Get the latest position Key reported from Tracking for IMEI
def get_last_position_key(an_imei):
    an_imei = an_imei = an_imei.replace('"', '')
    my_ref = 'Tracking/' + an_imei
    ref = fb.db.reference(my_ref)
    print('Getting position Key for IMEI: ', an_imei)

    if an_imei == '778499170000001':
        print('Test IMEI is invalid: ', an_imei, ' returning.')
        return ''
    
    snapshot = ref.order_by_child('date').limit_to_last(1).get()

    for key, val in snapshot.items():
        return key

    
# Get the current speed of a tracker
def get_current_speed(a_distance_from_last_point, a_time_from_last_point):
    if a_time_from_last_point == 0:
        return 0
    else:
        return round((a_distance_from_last_point / a_time_from_last_point), 4)


def get_triangle_heigth(redis, a_current_position, a_route_key):
    current_lat = a_current_position['latitud']
    current_lon = a_current_position['longitud']
    distance_list = []

    # Route caching begin
    if is_key_in_cache(redis, a_route_key, ROUTE_PREFIX):
        snapshot = json.loads(redis.get(ROUTE_PREFIX + a_route_key).decode())
    else:
        my_route_ref = fb.db.reference('Rutas/' + a_route_key)
        snapshot = my_route_ref.get()
        print('Storing snapshot for key: ', ROUTE_PREFIX + a_route_key, ' in cache')
        redis.set(ROUTE_PREFIX + a_route_key, json.dumps(my_route_snapshot))
    # Route caching end

    #route_ref = fb.db.reference(ROUTE_REFERENCE + a_route_key)
    #snapshot = route_ref.get()

    for index in snapshot['coords']:
        coord_lat = index['lat']
        coord_lon = index['lng']
        my_distance = get_distance(current_lat, current_lon, coord_lat, coord_lon)
        my_position_object = positions_object(coord_lat, coord_lon, my_distance)
        distance_list.append(my_position_object)
    distance_list.sort(key=lambda x: x.distance, reverse = False)
    my_closest_point = distance_list[0]
    my_second_closest_point = distance_list[1]

    hipotenusa = get_distance(my_closest_point.lat, my_closest_point.lon, my_second_closest_point.lat, my_second_closest_point.lon)

    triangle_height = .5 * (my_closest_point.distance * my_second_closest_point.distance) * (math.sin(hipotenusa))
    print('### Altura del triángulo (metros): ', round(triangle_height * 1000, 2))
    return round(triangle_height * 1000, 2)


def get_previous_record_from_ar(redis, a_tracker_id, a_task_key):
    #position_ref = 'AnalisisRuta/' + a_tracker_id + '/' + a_task_key
    #my_ref = fb.db.reference(position_ref)
    #position_snapshot = my_ref.order_by_child('date').limit_to_last(1).get()

    # Route Analysis caching begin
    if is_key_in_cache(redis, a_task_key, AR_PREFIX):
        task_cache = redis.get(AR_PREFIX + a_task_key)
        if task_cache == None:
            return None

        position_snapshot = json.loads(task_cache.decode())
    else:
        position_ref = 'AnalisisRuta/' + a_tracker_id + '/' + a_task_key
        my_ref = fb.db.reference(position_ref)
        position_snapshot = my_ref.order_by_child('date').limit_to_last(1).get()
        print('Storing snapshot for key: ', AR_PREFIX + a_task_key, ' in cache')
        print('** ** position snapshot: ', position_snapshot)
        if position_snapshot != None:
            for k, v in position_snapshot.items():
                snap = v

            print('*** Store in redis, position snap: ', snap)
            redis.set(AR_PREFIX + a_task_key, json.dumps(snap))
        else: return None
    # Route Analysis caching end

    return position_snapshot


def is_same_track_report(a_previous_date, a_current_date):
    if a_previous_date == a_current_date:
        print('Position is the same as previous')
        return True
    else:
        print('Position different than previous')
        return False


# stopped is defined by a differnece less than STOPPED_DISTANCE and
# different tracking reports
def is_stopped(a_current_position, a_previous_position, is_same_report):
    stopped = False
    current_lat  = a_current_position['latitud']
    current_lon  = a_current_position['longitud']
    previous_lat = a_previous_position['latitud']
    previous_lon = a_previous_position['longitud']
    my_distance = get_distance(current_lat, current_lon, previous_lat, previous_lon)

    if (my_distance * 1000) < STOPPED_DISTANCE and not is_same_report:
        stopped = True
    
    return stopped


def get_last_known_speed(a_ar_record):
    if 'last_known_speed' in a_ar_record:
        return a_ar_record['last_known_speed']
    else: 
        return 0


class positions_object():
    def __init__(self, lat, lon, distance):
        self.lat = lat
        self.lon = lon
        self.distance = distance


def get_time_difference(a_reference_time, a_actual_time):
    FMT = '%H:%M:%S'
    time_delta = datetime.datetime.strptime(a_reference_time, FMT) - datetime.datetime.strptime(a_actual_time, FMT)
    delta_seconds = time_delta.total_seconds()
    delta_minutes = delta_seconds / 60
    return delta_minutes

def is_key_in_cache(redis, a_key, a_prefix):
    is_present = False
    if redis.exists(a_prefix + a_key):
        print('Key: ', a_prefix + a_key, ' already exists in Redis, use from cache')
        is_present = True
    else:
        print('Key: ', a_prefix + a_key, ' not present in Redis')
    
    return is_present
