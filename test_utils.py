import utils

def test_calculate_route_time():
    assert utils.calculate_route_time(100, 80) == 1.25

def test_format_hour():
    assert utils.format_hour(0.0992) == '00:05:57'

def test_get_etd():
    assert utils.get_etd(100, 80) == '01:15:00'

def test_get_etd2():
    assert utils.get_etd(10, 50) == '00:12:00'

def test_get_etd_decimal1():
    assert utils.get_etd_decimal(100, 80) == 1.25

def test_get_etd_decimal2():
    assert utils.get_etd_decimal(10, 50) == 0.2

def test_get_rest_times_one():
    assert utils.get_rest_times(550, 80) == 1

def test_get_rest_times_two():
    assert utils.get_rest_times(850, 80) == 2

def test_get_rest_times_three():
    assert utils.get_rest_times(1200, 80) == 3

def test_get_advance_percentage_int():
    assert utils.get_advance_percentage(100, 200) == 50.00

def test_get_advance_percentage_float():
    assert utils.get_advance_percentage(300, 1555) == 80.71

def test_get_elapsed_time_hours():
    assert utils.get_elapsed_time_hours(1605662367, 1605662426) == '00:01:12'

def test_get_elapsed_time_decimal():
    assert utils.get_elapsed_time_decimal(1605662367, 1605662426) == 0.02

def test_get_decimal_time():
    assert utils.get_decimal_time('00:05:57') == 0.0992

def test_get_current_speed_1():
    assert utils.get_current_speed(100, 1) == 100

def test_get_current_speed_2():
    assert utils.get_current_speed(0.678, .01) == 67.8

def test_time_percentage():
    assert utils.get_time_percentage(0.5, 2) == 25.00

def test_time_percentage_1():
    assert utils.get_time_percentage(15, 30) == 50

def test_time_percentage_2():
    assert utils.get_time_percentage(0, 2) == 0

def test_get_etan_1():
    assert utils.get_eta_n('01:35:05', 1605662426) == 1605668131

def test_get_seconds_from_time():
    assert utils.time_string_to_seconds('01:35:05') == 5705

def test_get_distance_1():
    assert utils.get_distance(25.488068, -100.968819, 25.488033, -100.968910) == 0.009928494711925296

def test_is_stopped_true():
    assert utils.is_stopped({'latitud':25.488068,'longitud':-100.968819},{'latitud':25.488033,'longitud':-100.968910,'fecha':1605662426}, False) == True

def test_is_stopped_false():
    assert utils.is_stopped({'latitud':25.488068,'longitud':-100.968819},{'latitud':25.488033,'longitud':-100.968910,'fecha':1605662426}, True) == False

def test_get_advance_percentage_1():
    assert utils.get_advance_percentage(15, 20) == 25

def test_get_advance_percentage_2():
    assert utils.get_advance_percentage(5, 20) == 75

def test_decimal_to_time_string_1():
    assert utils.decimal_to_time_string(.5) == '00:30:00'

def test_decimal_to_time_string_2():
    assert utils.decimal_to_time_string(.2) == '00:12:00'

def test_decimal_to_time_string_3():
    assert utils.decimal_to_time_string(.0833) == '00:04:59'

def test_get_time_difference_1():
    assert utils.get_time_difference('00:10:00','00:11:00') == -1.0

def test_get_time_difference_2():
    assert utils.get_time_difference('00:15:00','00:13:00') == 2.0

def test_get_time_difference_3():
    assert utils.get_time_difference('01:33:00','01:35:00') == -2.0

def test_get_time_difference_4():
    assert utils.get_time_difference('02:10:00','02:12:00') == -2.0

def test_get_time_difference_5():
    assert utils.get_time_difference('02:10:00','02:10:00') == 0.0

def test_get_time_difference_6():
    assert utils.get_time_difference('00:48:00','00:45:00') == 3.0